import 'package:flutter/material.dart';
import 'package:pokemontcg_app/widget/cards_list.dart';

class HomePage extends StatefulWidget {
  static const String id = "home_screen";
  static const String title = "Home";

  @override
  _HomePage createState() => _HomePage();
}

class _HomePage extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(HomePage.title),
      ),
      body: ListView(
        children: <Widget>[
          CardList(imageUrl: 'Bleu', name: 'Mon pokemon', id: "12"),
          CardList(imageUrl: 'Bleu', name: 'Mon pokemon', id: "12"),
          CardList(imageUrl: 'Bleu', name: 'Mon pokemon', id: "12"),
          CardList(imageUrl: 'Bleu', name: 'Mon pokemon', id: "12"),
          CardList(imageUrl: 'Bleu', name: 'Mon pokemon', id: "12"),
          CardList(imageUrl: 'Bleu', name: 'Mon pokemon', id: "12"),
          CardList(imageUrl: 'Bleu', name: 'Mon pokemon', id: "12"),
          CardList(imageUrl: 'Bleu', name: 'Mon pokemon', id: "12"),
        ],
      ),
    );
  }
}
