import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:pokemontcg_app/model/poke_model.dart';

const String url = "https://api.pokemontcg.io/v1/cards";

Future<List<PokeModel>> getPokemon() async {
  var response = await http.get(url);

  // Map<String, dynamic> responseJson = jsonDecode(response.body);
  // PokeModel pokeModel = PokeModel.fromJSON(responseJson);

  // return pokeModel;
  if (response.statusCode == 200) {
    List<PokeModel> pokeModel = [];
    Map<String, dynamic> responseJSON = json.decode(response.body);
    responseJSON.forEach((key, value) {
      pokeModel.add(PokeModel.fromJSON(value));
    });
  }
}
