import 'package:flutter/material.dart';

class CardList extends StatefulWidget {
  final String imageUrl;
  final String id;
  final String name;

  const CardList({Key key, this.imageUrl, this.id, this.name})
      : super(key: key);

  @override
  _CardListState createState() => _CardListState();
}

class _CardListState extends State<CardList> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Container(
            height: 12.0,
          ),
          Row(
            children: [
              Container(
                margin: new EdgeInsets.symmetric(horizontal: 12.0),
                width: 75.0,
                height: 125.0,
                color: Colors.red,
              ),
              Expanded(
                flex: 2,
                child: Text(widget.name),
              ),
              Container(
                child: Text('Id : ' + widget.id),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
