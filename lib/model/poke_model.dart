class PokeModel {
  String name;
  String imageUrl;

  PokeModel(this.name, this.imageUrl);

  PokeModel.fromJSON(Map<String, dynamic> json) {
    this.name = json['name'];
    this.imageUrl = json['imageUrl'];
  }
}
